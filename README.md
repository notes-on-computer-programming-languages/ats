# ats

Typed call-by-value functional programming language inspired by ML. [ATS_(programming_language)](https://en.m.wikipedia.org/wiki/ATS_(programming_language))

![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=coq+agda+z3+hol-light+why3&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=2006-01-01&to_date=&hlght_date=&date_fmt=%25Y)
![Debian popularity](https://qa.debian.org/cgi-bin/popcon-png?packages=why3+hol88+minlog+ats2-lang+dafny+cafeobj+maud&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=&to_date=&hlght_date=&date_fmt=%25Y)

* [*Learn X in Y minutes, Where X=Standard ML*](https://learnxinyminutes.com/docs/standard-ml/)
* [ATS](https://packages.debian.org/en/sid/ats2-lang)
  * ["ATS is an ML without a runtime and an optional GC via libgc."](https://www.reddit.com/r/haskell/comments/d5d13i/is_it_possible_to_design_a_functional_language/)
    * [manpages.debian.org](https://manpages.debian.org/unstable/ats2-lang)
    * https://fr.slideshare.net/mirrorren/ats-programming

# Combinators for Parsing in CPS-style
* [*Effective ATS:
Combinators for Parsing in CPS-style*
  ](http://ats-lang.github.io/EXAMPLE/EFFECTIVATS/parsing-cmbntr-cps/main.html)
  (ats-lang.github.io)
* [*Effective ATS:
Combinators for Parsing in CPS-style*
  ](http://ats-lang.sourceforge.net/EXAMPLE/EFFECTIVATS/parsing-cmbntr-cps/index.html)
  (ats-lang.sourceforge.net)
* [*Continuation-passing style*
  ](https://en.m.wikipedia.org/wiki/Continuation-passing_style)
